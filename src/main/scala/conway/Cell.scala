package conway

sealed trait Cell
case object Dead extends Cell
case object Live extends Cell
