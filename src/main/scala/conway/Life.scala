package conway

trait Life[W] { self =>
  def next(w: W): W

  implicit class LifeExt(w: W) {
    def next: W = self.next(w)
  }
}
