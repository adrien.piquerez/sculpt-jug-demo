package conway

trait Neighborhood[W, Loc, Cell] { self =>
  def neighbors(w: W, loc: Loc): Seq[Cell]

  implicit class NeighborhoodExt(w: W) {
    def neighbors(loc: Loc): Seq[Cell] = self.neighbors(w, loc)
  }
}
