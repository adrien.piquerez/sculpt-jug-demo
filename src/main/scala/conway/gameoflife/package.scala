package conway

package object gameoflife {
  type Shape = (Int, Int)
  type Loc = (Int, Int)
  type World[W] = conway.World[W, Shape, Loc, Cell]
  type Neighborhood[W] = conway.Neighborhood[W, Loc, Cell]
}
