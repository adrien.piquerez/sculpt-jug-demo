package conway

trait Interaction[Cell] { self =>
  def interact(cell: Cell, neighbors: Seq[Cell]): Cell

  implicit class InteractionExt(cell: Cell) {
    def interact(neighbors: Seq[Cell]): Cell = self.interact(cell, neighbors)
  }
}
