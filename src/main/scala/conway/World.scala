package conway

trait World[W, Shape, Loc, Cell] { self =>
  def create(shape: Shape): W

  def shape(w: W): Shape
  def populate(w: W, loc: Loc): W
  def cell(w: W, loc: Loc): Cell

  implicit class WorldExt(w: W) {
    val shape: Shape = self.shape(w)
    def populate(loc: Loc): W = self.populate(w, loc)
    def cell(loc: Loc): Cell = self.cell(w, loc)
  }
}
