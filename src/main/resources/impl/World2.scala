import conway.{Cell, Dead, Live}
import conway.gameoflife.World

case class W(shape: (Int, Int), cells: Set[(Int, Int)])

object World3 extends World[W] {
  def create(shape: (Int, Int)): W = W(shape, Set())
  def shape(w: W): (Int, Int) = w.shape
  def populate(w: W, loc: (Int, Int)): W = w.copy(cells = w.cells + loc)
  def cell(w: W, loc: (Int, Int)): Cell = if (w.cells.contains(loc)) Live else Dead
}