import conway.{Cell, Dead, Live, Life, Interaction}
import conway.gameoflife.{Neighborhood, World}

class Life3[W](world: World[W], neighborhood: Neighborhood[W], interaction: Interaction[Cell]) extends Life[W] {
  import world._
  import neighborhood._
  import interaction._

  override def next(w: W): W = {
    val positions = for { i <- 0 until w.shape._1; j <- 0 until w.shape._2 } yield (i, j)
    positions.foldLeft(create(w.shape._1, w.shape._2)){ case (agg, (i, j)) =>
      w.cell(i, j).interact(w.neighbors(i, j)) match {
        case Live => agg.populate(i, j)
        case Dead => agg
      }
    }
  }
}
