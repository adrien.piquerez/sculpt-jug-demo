import conway.{Cell, Dead}
import conway.gameoflife.{World, Neighborhood}

class Neighborhood4[W](world: World[W]) extends Neighborhood[W] {
  import world._

  override def neighbors(w: W, loc: (Int, Int)): Seq[Cell] = {
    val (x, y) = loc
    for {
      i <- x - 1 to x + 1 if i >= 0 && i < w.shape._1
      j <- y - 1 to y + 1 if j >= 0 && j < w.shape._2
      if (i, j) != (x, y)
    } yield w.cell(i, j)
  }
}
